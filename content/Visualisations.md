+++
title = 'Visualisations'
date = 2024-01-31T11:41:32+01:00
draft = true
+++


## Examples

![](/testing_hugo/img/hist-evol-datasets-per-repo.png)

![](/testing_hugo/img/hist-quantity-year-type.png)

![](/testing_hugo/img/pie--datacite-client.png) 

![](/testing_hugo/img/pie--datacite-type.png)




## Sources

(so far) 

|    		|Dataset numbers      | UGA perimeter |
|-----------|---------------------|---------------|
|RDG		|42 |contact, auteurs, producteur et contributeurs avec "UGA" OR "Grenoble" |
|DataCite	|1247| creator et contributor avec ROR + clients & publisher|
|Zenodo		|1041|creator et contributor avec "grenoble"|
|Nakala		|32 |UGA user identifiers|
|BSO via HAL|32 |NA|
|... 		|   |

## test js
{{< bokeh >}}
